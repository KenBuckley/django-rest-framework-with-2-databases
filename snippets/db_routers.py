class AuthRouter:
    """
    A router to control all database operations on models in the
    auth application. Basically ensures auth goes to database 'db' only.
    """
    
    TARGET_DATABASE='db'  #sqlite
    APP_LABEL = 'auth'
    
    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go to auth_db.
        """
        if model._meta.app_label == self.APP_LABEL:
            return self.TARGET_DATABASE
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to auth_db.
        """
        if model._meta.app_label == self.APP_LABEL:
            return self.TARGET_DATABASE
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        if obj1._meta.app_label == self.APP_LABEL or \
           obj2._meta.app_label == self.APP_LABEL:
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth app only appears in the 'auth_db'
        database.
        """
        if app_label == self.APP_LABEL:
            return db == self.TARGET_DATABASE
        return None


class BertonDataRouter:
    TARGET_DATABASE = 'db'  # sqlite
    EXCLUDE_APP_LABEL = 'snippets'

    def db_for_read(self, model, **hints):
        """
        Reads from db2 only for snippets.
        """
        if model._meta.app_label not in [ self.EXCLUDE_APP_LABEL] :  #just exclude these apps
            return self.TARGET_DATABASE
        return None

    def db_for_write(self, model, **hints):
        """
        Writes always go to primary.
        """
        if model._meta.app_label not in [ self.EXCLUDE_APP_LABEL]:
            return self.TARGET_DATABASE
        return None


    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        All snippet models.
        """
        if app_label not in [ self.EXCLUDE_APP_LABEL]:
            return db == self.TARGET_DATABASE
        elif db == self.TARGET_DATABASE:
            # migrate everything where db='db' and app != snippets.
            return True

        # No opinion for all other scenarios
        return None



class BertonDataLogRouter:
    """
    only allow snippets tables to be written or read from db2,
    only
    processed by django.db.utils.
    """
    TARGET_DATABASE='db2'  #sqlite
    APP_LABEL = 'snippets'
    def db_for_read(self, model, **hints):
        """
        Reads from db2 only for snippets.
        """
        if model._meta.app_label == self.APP_LABEL:
            return self.TARGET_DATABASE
        return None # No opinion

    def db_for_write(self, model, **hints):
        """
        Writes always go to primary.
        """
        if model._meta.app_label == self.APP_LABEL:
            return self.TARGET_DATABASE
        return None # No opinion

    def allow_relation(self, obj1, obj2, **hints):
        """
        Relations between objects are allowed if both objects are
        in snippets.
        """

        if obj1._meta.app_label == self.APP_LABEL and \
           obj2._meta.app_label == self.APP_LABEL:
            return True
        # https://strongarm.io/blog/multiple-databases-in-django/
        # No opinion if neither object is in the Example app (defer to default or other routers).
        elif self.APP_LABEL not in [obj1._meta.app_label, obj2._meta.app_label]:
            return None # No opinion
        # default to block relationship if one object is in the Example app and the other isn't.
        return False

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        All snippet models.
        """

        if app_label == self.APP_LABEL and db == self.TARGET_DATABASE:
            return True
        elif app_label == self.APP_LABEL and db != self.TARGET_DATABASE:
            return False     #this can be shortened but I like the clarity of the code
        elif db == self.TARGET_DATABASE:
            # Ensure that all other apps don't get migrated on the target database.
            return False

        # No opinion for all other scenarios
        return None