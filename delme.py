#!/usr/bin/env python
import django
import os
import sys

if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'drfdemo.settings')
    django.setup()
    from django.contrib.auth.models import User

    for _ in range(10, 100):
        user = User.objects.create_user(username=f'u{_}',
                                        email=f'u{_}@gmail.com',
                                        password=f'u{_}a1234567')
